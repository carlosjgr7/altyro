import React from "react";
import OrderForm from "./components/OrderForm";

const NewOrder = () => {
  return (
    <div>
      <OrderForm />
    </div>
  );
};

export default NewOrder;
