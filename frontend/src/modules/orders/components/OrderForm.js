import React, { useState } from "react";
import { Formik } from "formik";
import { Form, Row, Col, Button } from "reactstrap";
// import * as Yup from "yup";
import InputText from "../../../components/Fields/InputText";
import InputPhone from "../../../components/Fields/InputPhone";
import InputSelect from "../../../components/Fields/InputSelect";
// import { errorRequired } from "../../../constants/inputs";
// import { getAsync } from "../../../services/amplifyServices";
// import InputSimpleImage from "../../../components/Fields/InputSimpleImage";
// import InputRange from "../../../components/Fields/InputRange";
import TopBarLoading from "../../../components/ui/TopBarLoading";

const OrderForm = ({ initialValues = {}, onSubmit, isEdit }) => {
  const [typeSubmit] = useState("Activa");

  // const validationSchema = Yup.object().shape({
  //   title: Yup.string().required(errorRequired),
  //   productId: Yup.string().required(errorRequired),
  //   image: Yup.mixed()
  //     .required(errorRequired)
  //     .nullable()
  // });

  return (
    <Formik
      initialValues={initialValues}
      // validationSchema={validationSchema}
      onSubmit={(values, actions) => {
        onSubmit(values, actions, typeSubmit);
      }}
      enableReinitialize
    >
      {({
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
        values,
        touched,
        errors,
        setFieldValue
        /* and other goodies */
      }) => {
        return (
          <Form onSubmit={handleSubmit} encType="multipart/form-data">
            <TopBarLoading
              isSubmitting={isSubmitting}
              touched={Object.keys(touched).length}
            />
            <Row>
              <Col xs="12" sm="12" lg="6">
                <InputSelect
                  type="select"
                  placeholder="Rubro de la empresa"
                  autoComplete="service"
                  name="service"
                  handleChange={handleChange}
                  handleBlur={handleBlur}
                  revision
                  value={values.service}
                  touched={touched.service}
                  error={errors.service}
                >
                  <option value="">Seleccione una opción</option>
                  {[
                    { _id: 1, name: "Servicio 1" },
                    { _id: 2, name: "Servicio 2" }
                  ].map(product => (
                    <option
                      key={`product${product._id}`}
                      value={product._id}
                      label={product.name}
                    />
                  ))}
                </InputSelect>
              </Col>
              <Col xs="12" sm="12" lg="6">
                <InputText
                  type="text"
                  placeholder="Nombre del cliente"
                  autoComplete="client"
                  name="client"
                  handleChange={handleChange}
                  handleBlur={handleBlur}
                  value={values.client}
                  error={errors.client}
                  touched={touched.client}
                />
              </Col>
              <Col xs="12" sm="12" lg="6">
                <InputText
                  type="text"
                  placeholder="Dirección"
                  autoComplete="direction"
                  name="direction"
                  handleChange={handleChange}
                  handleBlur={handleBlur}
                  value={values.direction}
                  error={errors.direction}
                  touched={touched.direction}
                />
              </Col>
              <Col xs="12" sm="12" lg="6">
                <InputPhone
                  name="phoneNumber"
                  value={values.phoneNumber}
                  error={errors.phoneNumber}
                  touched={touched.phoneNumber}
                  setFieldValue={setFieldValue}
                />
              </Col>
              <Col xs="12" sm="12" lg="6">
                <InputText
                  type="text"
                  placeholder="Observaciones"
                  autoComplete="observations"
                  name="observations"
                  handleChange={handleChange}
                  handleBlur={handleBlur}
                  value={values.observations}
                  error={errors.observations}
                  touched={touched.observations}
                />
              </Col>
            </Row>

            <Row>
              <Col xs="12" sm="12" lg="6" style={{ marginBottom: 15 }}>
                <Button
                  style={{ backgroundColor: "#a200bb", color: "#FFFFFF" }}
                  className="btn-pill btn-lg btn-block"
                  type="button"
                  onClick={() => {
                    // setTypeSubmit("Activa");
                    handleSubmit();
                  }}
                  disabled={isSubmitting}
                >
                  {isEdit ? "Actualizar" : "Guardar"}
                </Button>
              </Col>
            </Row>
          </Form>
        );
      }}
    </Formik>
  );
};

export default OrderForm;
