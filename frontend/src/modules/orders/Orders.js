import React from "react";
import { Row, Col, Card, CardBody, Table } from "reactstrap";

const Orders = () => {
  const orders = [
    {
      _id: "abbb1231aji13",
      client: "CLiente 1",
      direction: "Descripcion 2",
      phoneNumber: "56123123123",
      observations: "observación 1"
    },
    {
      _id: "abahjak2uh",
      client: "CLiente 2",
      direction: "Descripcion 3",
      phoneNumber: "56123123123",
      observations: "observación 2"
    },
    {
      _id: "abahj12312j",
      client: "CLiente 3",
      direction: "Descripcion 4",
      phoneNumber: "56123123122",
      observations: "observación 4"
    }
  ];
  return (
    <div>
      <Row>
        <Col xs="12">
          <Card>
            <CardBody>
              <div>
                <Table responsive striped>
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>cliente</th>
                      <th>diracción</th>
                      <th>numero</th>
                      <th>observaciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    {orders.map(order => {
                      return (
                        <tr key={`oreder_${order._id}`}>
                          <td>{order._id}</td>
                          <td>{order.client}</td>
                          <td>{order.direction}</td>
                          <td>{order.phoneNumber}</td>
                          <td>{order.observations}</td>
                        </tr>
                      );
                    })}
                  </tbody>
                </Table>
              </div>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default Orders;
