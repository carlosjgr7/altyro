/* eslint-disable jsx-a11y/alt-text */
import React from "react";
import "../../scss/bodyBg.scss";
import FormUser from "./components/FormUser";
import logoLarge from "../../assets/img/brand/logo.png";
// import { postAsync } from "../../services/amplifyServices";
// import { notification } from "../../utils/notification";
import ContainerCenter from "../../components/ui/ContainerCenter";
import CardMaterial from "../../components/Cards/CardMaterial";
import "./SignUp.scss";

const SignUp = ({ history }) => {
  const initialValues = {
    phone_number: "56",
    email: "",
    shopRut: "",
    shopName: "",
    name: "",
    rut: "",
    workersQty: "",
    categoryId: "",
    logo: undefined,
    terms_conditions: null
  };

  const onSubmit = async (values, actions) => {
    console.log("values", values);
    history.goBack();
    // try {
    //   // if (values.logo) {
    //   //   const file = await UploadSingleFile(values.logo);
    //   //   values.logo = file.name;
    //   // }
    //   const data = await postAsync("/register/store", values);
    //   if (("data", data)) {
    //     if (data.message) {
    //       throw new Error(data.message);
    //     }
    //     if (data.user) {
    //       notification("success", "Solicitud enviada").show();
    //       history.goBack();
    //     }
    //   } else {
    //     throw new Error("error de servidor");
    //   }
    //   actions.setSubmitting(false);
    // } catch (error) {
    //   notification("error", error.message, 5000).show();
    //   actions.setSubmitting(false);
    // }
  };

  return (
    <ContainerCenter numXs={10} numMd={5}>
      <CardMaterial>
        <img
          src={logoLarge}
          className="img-field"
          style={{ width: "50%", "margin-left": "24%" }}
          alt="logo"
        />
        <label className="text-center float-center" style={{ width: "100%" }}>
          Completa tus datos para registrarte
        </label>
        <div className="form-register">
          <FormUser onSubmit={onSubmit} initialValues={initialValues} />
        </div>
      </CardMaterial>
    </ContainerCenter>
  );
};

export default SignUp;
