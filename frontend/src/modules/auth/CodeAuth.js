import React, { useState } from "react";
import { Form, Row, Col, Button } from "reactstrap";
import { Formik } from "formik";
// import { useToasts } from "react-toast-notifications";
// import { useSelector, useDispatch } from "react-redux";
import { Box } from "@material-ui/core";
import TopBarLoading from "../../components/ui/TopBarLoading";
// import Routes from "../../constants/routes";
// import {
//   confirmSignUp,
//   resendConfirmCode,
//   sendCodeForgotPassword,
//   confirmCode,
//   getCurrentUser,
//   signIn
// } from "../../services/amplifyServices";
// import AuthActionsCreator from "../../store/actions/authActions";
// import errors from "../../constants/errors";
import logoLarge from "../../assets/img/brand/logo.png";
import ContainerCenter from "../../components/ui/ContainerCenter";
import CardMaterial from "../../components/Cards/CardMaterial";
import FieldText from "../../components/Fields/FieldText";
import ButtonRounded from "../../components/Buttons/ButtonRounded";

const CodeAuth = ({ history, match, location }) => {
  // const { password } = location.state;
  // const { addToast } = useToasts();
  const [email, setEmail] = useState(match.params.email);
  const [statusCode, setStatusCode] = useState(
    match.params.statusCode === "confirm" ? true : false
  );
  // const dispatch = useDispatch();
  // const auth = useSelector(state => state.auth);

  // useEffect(() => {
  //   if (auth._id) {
  //     history.push("/company/dashboard");
  //   }
  // }, [auth, history]);

  // const login = actions => {
    // signIn(
    //   email,
    //   password,
    //   async () => {
    //     const cUser = await getCurrentUser();
    //     actions.setSubmitting(false);
    //     dispatch(
    //       AuthActionsCreator.setCurrentUser({
    //         ...cUser,
    //         roleAuth: cUser.role[0]
    //       })
    //     );
    //   },
    //   reason => {
    //     actions.setSubmitting(false);
    //     if (reason && reason.message) {
    //       addToast(errors[reason.code], {
    //         appearance: "error",
    //         autoDismiss: true
    //       });
    //       history.push(Routes.SIGN_IN);
    //     }
    //   }
    // );
  // };

  const confirmCodeSignUp = (values, actions) => {
    // confirmSignUp(
    //   email,
    //   values.code,
    //   () => {
    //     addToast("Su cuenta ha sido registrada", {
    //       appearance: "success",
    //       autoDismiss: true
    //     });
    //     if (!password) {
    //       history.push(Routes.SIGN_IN);
    //     } else {
    //       login(actions);
    //     }
    //   },
    //   reason => {
    //     actions.setSubmitting(false);
    //     actions.resetForm();
    //     if (reason && reason.message) {
    //       addToast(reason.message, {
    //         appearance: "error",
    //         autoDismiss: true
    //       });
    //     }
    //   }
    // );
  };

  const sendCodeEmail = (values, actions) => {
    // sendCodeForgotPassword(values.email)
    //   .then(() => {
    //     actions.setSubmitting(false);
    //     actions.resetForm();
    //     setEmail(values.email);
    //     setStatusCode(true);
    //   })
    //   .catch(error => {
    //     actions.setSubmitting(false);
    //     actions.resetForm();
    //     addToast(error.message, {
    //       appearance: "error",
    //       autoDismiss: true
    //     });
    //   });
  };

  const confirmCodeEmail = (values, actions) => {
    // confirmCode(email, values.code, values.password)
    //   .then(() => {
    //     addToast("Su contraseña ha sido cambiada de forma exitosa", {
    //       appearance: "success",
    //       autoDismiss: true
    //     });
    //     history.push(Routes.SIGN_IN);
    //   })
    //   .catch(error => {
    //     actions.setSubmitting(false);
    //     actions.resetForm();
    //     addToast(error.message, {
    //       appearance: "error",
    //       autoDismiss: true
    //     });
    //   });
  };

  return (
    <ContainerCenter numXs={10} numMd={5}>
      <CardMaterial>
        <img
          src={logoLarge}
          className="img-field"
          style={{ width: "50%", "margin-left": "24%" }}
          alt="logo"
        />
        <div style={{ paddingLeft: 90, paddingRight: 90 }}>
          <Formik
            initialValues={{ email: email, code: "", password: "" }}
            onSubmit={(values, actions) => {
              if (email && match.params.statusCode === "confirm") {
                confirmCodeSignUp(values, actions);
              } else if (!email && match.params.statusCode === "forgot") {
                sendCodeEmail(values, actions);
              } else if (email && match.params.statusCode === "forgot") {
                confirmCodeEmail(values, actions);
              }
            }}
          >
            {({
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
              touched,
              errors,
              values
              /* and other goodies */
            }) => (
              <Form onSubmit={handleSubmit}>
                <TopBarLoading
                  isSubmitting={isSubmitting}
                  touched={Object.keys(touched).length}
                />
                {email ? (
                  <div>
                    <h2 className="text-center">{email}</h2>
                    <Col className="text-center">
                      <Button
                        color="link"
                        type="button"
                        onClick={() => {
                          setEmail(null);
                          setStatusCode(false);
                        }}
                      >
                        Cambiar Correo
                      </Button>
                    </Col>
                  </div>
                ) : (
                  <div>
                    <label
                      className="text-center float-center"
                      style={{ width: "100%" }}
                    >
                      Ingresa tu correo de recuperación
                    </label>
                  </div>
                )}
                {!statusCode ? (
                  <Row>
                    <Col xs="12" sm="12" lg="12">
                      <FieldText
                        type="text"
                        label="Correo"
                        autoComplete="email"
                        name="email"
                        handleChange={handleChange}
                        handleBlur={handleBlur}
                        iconName="fa fa-envelope fa-lg"
                      />
                    </Col>
                  </Row>
                ) : (
                  <Row>
                    <Col xs="12" sm="12" lg="12">
                      <FieldText
                        type="text"
                        label="Código"
                        autoComplete="code"
                        name="code"
                        handleChange={handleChange}
                        handleBlur={handleBlur}
                        error={errors.code}
                        touched={touched.code}
                        value={values.code}
                        iconName="fa fa-lock fa-lg"
                      />
                    </Col>
                    {match.params.statusCode === "forgot" && (
                      <Col xs="12" sm="12" lg="12">
                        <FieldText
                          type="password"
                          label="Contraseña"
                          autoComplete="password"
                          name="password"
                          handleChange={handleChange}
                          handleBlur={handleBlur}
                          iconName="fa fa-key fa-lg"
                        />
                      </Col>
                    )}
                  </Row>
                )}
                {email && (
                  <Row>
                    <Col className="text-center">
                      <Button
                        style={{ backgroundColor: "#a200bb", color: "#FFFFFF" }}
                        color="link"
                        type="button"
                        onClick={() => {
                          // resendConfirmCode(email);
                        }}
                      >
                        Solicitar nuevo código
                      </Button>
                    </Col>
                  </Row>
                )}
                <Box display="flex" justifyContent="center" m={1} p={1}>
                  <ButtonRounded
                    style={{ backgroundColor: "#a200bb", color: "#FFFFFF" }}
                    text="Enviar"
                    type="submit"
                    disabled={isSubmitting}
                  />
                </Box>
              </Form>
            )}
          </Formik>
        </div>
      </CardMaterial>
    </ContainerCenter>
  );
};

export default CodeAuth;
