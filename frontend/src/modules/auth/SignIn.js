/* eslint-disable jsx-a11y/alt-text */
import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import { Button } from "reactstrap";
// import { useToasts } from "react-toast-notifications";
// import { useDispatch } from "react-redux";
import logoLarge from "../../assets/img/brand/logo.png";
import FormSigIn from "./components/FormSigIn";
// import {
  // getCurrentUser,
  // signIn,
  // signOut,
  // resendConfirmCode
// } from "../../services/amplifyServices";
// import AuthActionsCreator from "../../store/actions/authActions";
// import errors from "../../constants/errors";
import ContainerCenter from "../../components/ui/ContainerCenter";
import CardMaterial from "../../components/Cards/CardMaterial";

const SignIn = ({ history }) => {
  // const { addToast } = useToasts();
  // const [currentUser, setCurrentUser] = useState(false);
  // const dispatch = useDispatch();
  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);


  const loginWith = roleAuth => {
    history.push("company/dashboard");
  };

  const logout = async () => {
    // await signOut();
    toggle();
  };

  const onSubmit = async (values, actions) => {
    // const { email, password } = values;
    toggle();
  };

  return (
    <ContainerCenter numXs={10} numMd={5}>
      <CardMaterial>
        <img src={logoLarge} className="img-field center" style={{ width: "50%", "margin-left": "24%" }} />

        <label className="text-center float-center" style={{ width: "100%" }}>
          Ingresa tus credenciales para acceder
        </label>

        <FormSigIn onSubmit={onSubmit} history={history} />

        <br />
        <br />
        <label className="text-center float-center" style={{ width: "100%" }}>
          ¿No tienes cuenta en Altyro?{" "}
          <Link to="/signUp">
            <Button color="link" className="px-0" style={{ color: "#a200bb" }}>
              Registrate
            </Button>
          </Link>
        </label>
      </CardMaterial>
      <div>
        <Modal isOpen={modal} toggle={toggle}>
          <ModalHeader toggle={logout}>Iniciar sesion como</ModalHeader>
          <ModalBody>
            Su cuenta posee 2 roles de usuario una como administrador y una como
            tienda ¿con que rol desea ingresar a la cuenta?
          </ModalBody>
          {modal && (
            <ModalFooter>
              <Button
                color="primary"
                type="button"
                onClick={() => loginWith("admin")}
              >
                administrador
              </Button>
              <Button
                color="success"
                type="button"
                onClick={() => loginWith("store")}
              >
                tienda
              </Button>
              <Button color="secondary" type="button" onClick={logout}>
                Cancelar
              </Button>
            </ModalFooter>
          )}
        </Modal>
      </div>
    </ContainerCenter>
  );
};

export default SignIn;
