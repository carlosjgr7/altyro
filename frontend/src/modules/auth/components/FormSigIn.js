import React from "react";
import { Row, Col, Button, Form } from "reactstrap";
import { Box } from "@material-ui/core";
import { Formik } from "formik";
import TopBarLoading from "../../../components/ui/TopBarLoading";
import FieldText from "../../../components/Fields/FieldText";
import ButtonRounded from "../../../components/Buttons/ButtonRounded";
import "./formSigIn.scss";

const FormSigIn = ({ onSubmit, history }) => {
  const toForgot = () => {
    console.log('aqui');
    history.push("/code/forgot", {});
  };
  return (
    <Formik
      initialValues={{ email: "", password: "" }}
      onSubmit={(values, actions) => {
        onSubmit(values, actions);
      }}
    >
      {({
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
        touched
        /* and other goodies */
      }) => (
        <Form onSubmit={handleSubmit}>
          <TopBarLoading
            isSubmitting={isSubmitting}
            touched={Object.keys(touched).length}
          />
          <div className="form-sign">
            <FieldText
              type="text"
              placeholder="Correo"
              autoComplete="email"
              name="email"
              handleChange={handleChange}
              handleBlur={handleBlur}
              iconName="fa fa-user fa-lg"
              label="Correo"
            />
            <FieldText
              type="password"
              label="Contraseña"
              placeholder="Contraseña"
              autoComplete="password"
              name="password"
              handleChange={handleChange}
              handleBlur={handleBlur}
              iconName="fa fa-key fa-lg"
            />
          </div>

          <br />
          <br />
          <Row
            style={{ paddingLeft: 60, paddingRight: 60 }}
            className="hidden-loggin"
          >
            <Col xs="12" sm="12" md="8">
              <Button color="link" className="px-0" onClick={toForgot} style={{ color: "#a200bb" }}>
                ¿Olvidaste tu contraseña?
              </Button>
            </Col>
            
            <Col xs="12" sm="12" md="4">
              <ButtonRounded
                style={{ backgroundColor: "#a200bb", color: "#FFFFFF" }}
                text="Enviar"
                type="submit"
                extraClass="float-button"
              />
              {/* <Button
                color="success btn-pill"
                style={{
                  backgroundColor: primaryOvnix,
                  color: "black",
                  ...elevationShadowStyle(3)
                }}
                className="px-4 float-right"
                type="submit"
                disabled={isSubmitting}
              >
                Enviar
              </Button> */}
            </Col>
          </Row>
          <div className="hidden-mobile">
            <Box display="flex" justifyContent="center" m={1} p={1}>
              <ButtonRounded
                text="Enviar"
                type="submit"
                extraClass="float-button"
              />
            </Box>
            <Box display="flex" justifyContent="center" m={1} p={1}>
              <Button color="link" className="px-0" onClick={toForgot}>
                ¿Olvidaste tu contraseña?
              </Button>
            </Box>
          </div>
        </Form>
      )}
    </Formik>
  );
};

export default FormSigIn;
