import React, { useEffect } from "react";
import { Row, Col, Form } from "reactstrap";
import { Box } from "@material-ui/core";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { Formik } from "formik";
import * as Yup from "yup";
import {
  errorEmail,
  errorRequired,
  validatePhone,
  errorPhone
} from "../../../constants/inputs";
import CategoriesActionsCreator from "../../../store/actions/categoriesActions";
import InputSimpleImage from "../../../components/Fields/InputSimpleImage";
import TopBarLoading from "../../../components/ui/TopBarLoading";
import Routes from "../../../constants/routes";
import FieldText from "../../../components/Fields/FieldText";
import FieldSelect from "../../../components/Fields/FieldSelect";
import ButtonRounded from "../../../components/Buttons/ButtonRounded";
// import InputRadio from "../../../components/Fields/InputRadio";

const FormUser = ({ onSubmit, initialValues, isEdit }) => {
  const dispatch = useDispatch();
  // const categories = useSelector(state => state.categories);
  // const FILE_SIZE = 160 * 1024;
  // const SUPPORTED_FORMATS = [
  //   "image/jpg",
  //   "image/jpeg",
  //   "image/gif",
  //   "image/png"
  // ];

  useEffect(() => {
    dispatch(CategoriesActionsCreator.fetchCategories());
  }, [dispatch]);

  const validationSchema = Yup.object().shape({
    nameStore: Yup.string().required(errorRequired),
    rut: Yup.string().required(errorRequired),
    shopRut: Yup.string().required(errorRequired),
    direction: Yup.string().required(errorRequired),
    email: Yup.string()
      .email(errorEmail)
      .required(errorRequired),
    shopName: Yup.string().required(errorRequired),
    nameUser: Yup.string().required(errorRequired),
    phone_number: Yup.string()
      .required()
      .matches(validatePhone, errorPhone),
    categoryId: Yup.string().required(errorRequired),
    description: Yup.string().required(errorRequired),
    instagramUser: Yup.string().required(errorRequired)
  });

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={(values, actions) => {
        onSubmit(values, actions);
      }}
      enableReinitialize
    >
      {({
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
        values,
        touched,
        errors,
        setFieldValue
        /* and other goodies */
      }) => {
        return (
          <Form onSubmit={handleSubmit}>
            <TopBarLoading
              isSubmitting={isSubmitting}
              touched={Object.keys(touched).length}
            />
            <Row>
              <Col xs="12" sm="12" lg="12">
                <FieldText
                  type="text"
                  label="Nombre de la tienda"
                  autoComplete="nameStore"
                  name="nameStore"
                  handleChange={handleChange}
                  handleBlur={handleBlur}
                  error={errors.nameStore}
                  touched={touched.nameStore}
                  value={values.nameStore}
                  disabled={isEdit}
                />
              </Col>
              <Col xs="12" sm="12" lg="12">
                <FieldText
                  type="text"
                  label="RUT del Representante"
                  autoComplete="rut"
                  name="rut"
                  handleChange={handleChange}
                  handleBlur={handleBlur}
                  error={errors.rut}
                  touched={touched.rut}
                  value={values.rut}
                />
              </Col>
            </Row>

            <Row>
              <Col xs="12" sm="12" lg="12">
                <FieldText
                  type="text"
                  label="RUT empresa"
                  autoComplete="shopRut"
                  name="shopRut"
                  handleChange={handleChange}
                  handleBlur={handleBlur}
                  error={errors.shopRut}
                  touched={touched.shopRut}
                  value={values.shopRut}
                />
              </Col>
              <Col xs="12" sm="12" lg="12">
                <FieldText
                  type="text"
                  label="Dirección"
                  autoComplete="direction"
                  name="direction"
                  handleChange={handleChange}
                  handleBlur={handleBlur}
                  error={errors.direction}
                  touched={touched.direction}
                  value={values.direction}
                />
              </Col>
            </Row>

            <Row>
              <Col xs="12" sm="12" lg="12">
                <FieldText
                  type="text"
                  label="Nombre del encargado"
                  autoComplete="nameUser"
                  name="nameUser"
                  handleChange={handleChange}
                  handleBlur={handleBlur}
                  error={errors.nameUser}
                  touched={touched.nameUser}
                  value={values.nameUser}
                />
              </Col>
              <Col xs="12" sm="12" lg="12">
                <FieldText
                  type="text"
                  label="Correo"
                  autoComplete="email"
                  name="email"
                  handleChange={handleChange}
                  handleBlur={handleBlur}
                  error={errors.email}
                  touched={touched.email}
                  value={values.email}
                  disabled={isEdit}
                />
              </Col>
              <Col xs="12" sm="12" lg="12">
                <FieldSelect
                  type="select"
                  label="Categoria"
                  autoComplete="categoryId"
                  name="categoryId"
                  handleChange={handleChange}
                  handleBlur={handleBlur}
                  error={errors.categoryId}
                  touched={touched.categoryId}
                  value={values.categoryId}
                  options={[
                    { _id: 1, name: "Categoria 1" },
                    { _id: 2, name: "Categoria 2" }
                  ]}
                  optionKey="_id"
                  optionValue="name"
                />
              </Col>
            </Row>

            <Row>
              <Col xs="12" sm="12" lg="12">
                <FieldText
                  type="text"
                  label="Breve reseña de la venta"
                  autoComplete="description"
                  name="description"
                  handleChange={handleChange}
                  handleBlur={handleBlur}
                  error={errors.description}
                  touched={touched.description}
                  value={values.description}
                  disabled={isEdit}
                />
              </Col>
              <Col xs="12" sm="12" lg="12">
                <FieldText
                  type="text"
                  label="Número de teléfono"
                  autoComplete="phone_number"
                  name="phone_number"
                  handleChange={handleChange}
                  handleBlur={handleBlur}
                  error={errors.phone_number}
                  touched={touched.phone_number}
                  value={values.phone_number}
                  disabled={isEdit}
                />
              </Col>
              <Col xs="12" sm="12" lg="12">
                <FieldText
                  type="text"
                  label="Usuario de instagram"
                  autoComplete="instagramUser"
                  name="instagramUser"
                  handleChange={handleChange}
                  handleBlur={handleBlur}
                  error={errors.instagramUser}
                  touched={touched.instagramUser}
                  value={values.instagramUser}
                  disabled={isEdit}
                />
              </Col>
              {isEdit && (
                <Col xs="12" sm="12" lg="6">
                  <InputSimpleImage
                    name="logo"
                    value={values.logo}
                    setFieldValue={setFieldValue}
                    error={errors.logo}
                    touched={touched.logo}
                    placeholder="Cargar logo en formato PNG o jpg"
                  />
                </Col>
              )}
            </Row>

            <Box display="flex" justifyContent="center" m={1} p={1}>
              {isEdit && (
                <Link to={Routes.CHANGE_PASSWORD}>
                  <ButtonRounded
                    style={{ backgroundColor: "#a200bb", color: "#FFFFFF" }}
                    text="Cambiar contraseña"
                    disabled={isSubmitting}
                  />
                  &nbsp;&nbsp;
                </Link>
              )}
              <ButtonRounded
                style={{ backgroundColor: "#a200bb", color: "#FFFFFF" }}
                text={isEdit ? "Actualizar información" : "Registrar"}
                type="submit"
                disabled={isSubmitting}
              />
            </Box>
          </Form>
        );
      }}
    </Formik>
  );
};

export default FormUser;
