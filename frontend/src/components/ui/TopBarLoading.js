import React, { useRef, useEffect } from "react";
import LoadingBar from "react-top-loading-bar";

const TopBarLoading = ({ isSubmitting, touched }) => {
  const LoadingBarRef = useRef(null);
  useEffect(() => {
    if (LoadingBarRef.current) {
      if (touched > 0) {
        if (isSubmitting) {
          LoadingBarRef.current.continuousStart();
        } else {
          LoadingBarRef.current.complete();
        }
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isSubmitting]);

  return (
    <div>
      <LoadingBar ref={LoadingBarRef} color="#20a8d8" />
    </div>
  );
};

export default TopBarLoading;
