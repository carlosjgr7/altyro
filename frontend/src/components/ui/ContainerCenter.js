import React from "react";
import { Grid } from "@material-ui/core";

const ContainerCenter = ({ children, numXs = 10, numMd = 6 }) => {
  return (
    <Grid
      container
      spacing={0}
      direction="column"
      alignItems="center"
      justify="center"
      style={{ minHeight: "100vh" }}>
      <Grid item xs={numXs} md={numMd}>
        {children}
      </Grid>
    </Grid>
  );
};

export default ContainerCenter;
