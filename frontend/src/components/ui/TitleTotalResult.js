import React from "react";
import { Col } from "reactstrap";

const TitleTotalResult = ({ title, color = "info", result }) => {
  return (
    <Col sm="4">
      <div className={`callout callout-${color}`}>
        <small className="text-muted">{title}</small>
        <br />
        <div>
          <strong className="h4">{result}</strong>
        </div>
      </div>
    </Col>
  );
};

export default TitleTotalResult;
