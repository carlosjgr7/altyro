import React, { useState, useEffect } from "react";
// import { Storage } from "aws-amplify";

const Thumb = ({ file, setFieldValue, values, name, notDelete }) => {
  const [loading] = useState(false);
  const [thumb, setThumb] = useState(null);

  useEffect(() => {
    if (file && typeof file === "object") {
      let reader = new FileReader();
      reader.onloadend = () => {
        setThumb(reader.result);
      };
      reader.readAsDataURL(file);
    } else if (file && typeof file === "string") {
      // Storage.get(file).then(data => setThumb(data));
    }
  }, [file]);
  if (!file) {
    return null;
  }

  if (loading) {
    return <p>loading...</p>;
  }

  return (
    <div>
      {!notDelete && (
        <i
          className="fa fa-close fa-lg"
          style={{ position: "absolute", top: 15, right: 33 }}
          onClick={e => {
            e.preventDefault();
            setFieldValue(
              name,
              values.filter(v => {
                if (typeof file === "string") {
                  return typeof v === "string" ? v !== file : true;
                } else if (typeof file === "object") {
                  return typeof v === "object" ? v.path !== file.path : true;
                }
                return true;
              }),
            );
          }}
        />
      )}
      <img
        src={thumb}
        alt={file.name}
        className="img-thumbnail mt-2"
        height={200}
        width={200}
        style={{ width: 200, height: 200, marginRight: 10 }}
      />
    </div>
  );
};

export default Thumb;
