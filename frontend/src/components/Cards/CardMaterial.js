import React from "react";
import { Card, CardContent } from "@material-ui/core";

const CardMaterial = ({ children }) => {
  return (
    <Card style={{ width: "100%" }}>
      <CardContent>{children}</CardContent>
    </Card>
  );
};

export default CardMaterial;
