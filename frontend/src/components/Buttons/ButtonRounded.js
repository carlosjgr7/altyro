import React from "react";
import { Button } from "@material-ui/core";
import { primaryOvnix } from "../../styles/palette";

const ButtonRounded = ({
  type = "button",
  text,
  color = primaryOvnix,
  onClick,
  disabled,
  extraClass
}) => {
  return (
    <Button
      type={type}
      variant="contained"
      className={`px-4 ${extraClass}`}
      style={{
        backgroundColor: color,
        color: "black",
        borderRadius: 30,
        fontWeight: 350,
        textTransform: "capitalize"
      }}
      onClick={onClick}
      disabled={disabled}
    >
      {text}
    </Button>
  );
};

export default ButtonRounded;
