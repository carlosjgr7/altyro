import React from "react";
import {
  FormControl,
  InputLabel,
  Select,
  FormHelperText,
  MenuItem
} from "@material-ui/core";

const FieldSelect = ({
  autoComplete,
  name,
  handleChange,
  handleBlur,
  error,
  touched,
  multiple,
  value,
  options,
  optionKey,
  optionValue,
  label
}) => {
  return (
    <FormControl style={{ width: "100%", marginBottom: 20 }}>
      <InputLabel id="demo-simple-select-label">{label}</InputLabel>
      <Select
        labelId="demo-simple-select-label"
        id="demo-simple-select"
        autoComplete={autoComplete}
        name={name}
        onChange={handleChange}
        onBlur={handleBlur}
        invalid={error && touched}
        multiple={multiple}
        value={value}
        error={error && touched}
      >
        {options.map(option => (
          <MenuItem
            key={`category_${option[optionKey]}`}
            value={option[optionKey]}
          >
            {option[optionValue]}
          </MenuItem>
        ))}
      </Select>
      {error && touched && (
        <FormHelperText id="component-error-text">{error}</FormHelperText>
      )}
    </FormControl>
  );
};

export default FieldSelect;
