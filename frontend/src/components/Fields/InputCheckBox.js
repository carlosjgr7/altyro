import React from "react";
import { InputGroup, Input, FormFeedback, FormGroup, Label } from "reactstrap";

const InputCheckbox = ({
  type = "text",
  placeholder,
  autoComplete,
  name,
  handleChange,
  handleBlur,
  error,
  touched,
  label,
  value,
}) => {
  let checked = false;
  if(value){
    if(value.length > 0)
       checked = true;
  }
  return (
    <FormGroup check>
      <InputGroup className="mb-3">
        <Input
          type={type}
          placeholder={placeholder}
          autoComplete={autoComplete}
          name={name}
          onChange={handleChange}
          onBlur={handleBlur}
          invalid={error && touched}
          defaultChecked={checked}
        />
        <Label>{label}</Label>
        <FormFeedback>{error}</FormFeedback>
      </InputGroup>
    </FormGroup>
  );
};

export default InputCheckbox;
