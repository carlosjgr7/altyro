import React from "react";
import {
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Input,
  FormFeedback,
  FormGroup,
} from "reactstrap";

const InputDate = ({
  type = "text",
  placeholder,
  autoComplete,
  name,
  handleChange,
  handleBlur,
  error,
  touched,
  iconName
}) => {
  return (
    <FormGroup>
      <InputGroup className="mb-3">
        {iconName && (
          <InputGroupAddon addonType="prepend">
            <InputGroupText>
              <i className={iconName}></i>
            </InputGroupText>
          </InputGroupAddon>
        )}
        <Input
          type={type}
          placeholder={placeholder}
          autoComplete={autoComplete}
          name={name}
          onChange={handleChange}
          onBlur={handleBlur}
          invalid={error && touched}
        />
        <FormFeedback>{error}</FormFeedback>
      </InputGroup>
    </FormGroup>
  );
};

export default InputDate;
