import React from "react";
import { FormGroup, InputGroup, Input } from "reactstrap";
import Thumb from "../ui/Thumb";

const InputSimpleImage = ({
  value,
  setFieldValue,
  name,
  error,
  touched,
  placeholder,
  disabled
}) => {
  return (
    <FormGroup>
      <InputGroup className="input-group">
        <div className="input-group-prepend">
          <span className="input-group-text" id="inputGroupFileAddon01">
            <i className="fa fa-picture-o" />
          </span>
        </div>
        <div className="custom-file">
          <Input
            type="file"
            className="custom-file-input"
            id="inputGroupFile01"
            aria-describedby="inputGroupFileAddon01"
            onChange={e => {
              const file = e.target.files[0];
              setFieldValue(name, file);
            }}
            invalid={error && touched}
            disabled={disabled}
          />
          <label className="custom-file-label" htmlFor="inputGroupFile01">
            {value ? value.name : placeholder || "Seleccione una foto o video"}
          </label>
        </div>
      </InputGroup>
      {value && (
        <div>
          {value.type.includes("image") ? (
            <Thumb
              file={value.size ? value : value.name}
              setFieldValue={setFieldValue}
              name={name}
              notDelete
            />
          ) : null}
        </div>
      )}
      {error && touched && (
        <p
          className="text-danger"
          style={{ marginTop: "0.25rem", fontSize: "80%" }}
        >
          {error}
        </p>
      )}
    </FormGroup>
  );
};

export default InputSimpleImage;
