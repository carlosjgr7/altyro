import React from "react";
import Moment from "moment";
import momentLocalizer from "react-widgets-moment";
import DateTimePicker from "react-widgets/lib/DateTimePicker";
import "react-widgets/dist/css/react-widgets.css";

Moment.locale("en");
momentLocalizer();

const InputDateTime = ({ name, error, value, setFieldValue, touched }) => {
  return (
    <div>
      <DateTimePicker
        placeholder={"yyyy/mm/dd hh:mm"}
        value={value ? new Date(value) : null}
        format="YYYY/MM/DD hh:mm"
        onChange={value => setFieldValue(name, value)}
      />
      {error && touched && (
        <p
          className="text-danger"
          style={{ marginTop: "0.25rem", fontSize: "80%" }}
        >
          {error}
        </p>
      )}
    </div>
  );
};

export default InputDateTime;
