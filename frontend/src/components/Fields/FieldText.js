import React from "react";

import {
  FormControl,
  InputLabel,
  Input,
  InputAdornment,
  FormHelperText
} from "@material-ui/core";
import { primaryOvnix } from "../../styles/palette";

const FieldText = ({
  type = "text",
  placeholder,
  autoComplete,
  name,
  handleChange,
  handleBlur,
  error,
  touched,
  iconName,
  value,
  disabled,
  extraStyle = {},
  label
}) => {
  return (
    <FormControl style={{ width: "100%", marginBottom: 15 }}>
      <InputLabel htmlFor="standard-adornment-password">{label}</InputLabel>
      <Input
        type={type}
        placeholder={placeholder}
        autoComplete={autoComplete}
        name={name}
        onChange={handleChange}
        onBlur={handleBlur}
        invalid={error && touched}
        disabled={disabled}
        style={extraStyle}
        value={value}
        error={error && touched}
        endAdornment={
          iconName ? (
            <InputAdornment position="end">
              <i className={iconName} style={{ color: primaryOvnix }}></i>
            </InputAdornment>
          ) : null
        }
      />
      {error && touched && (
        <FormHelperText id="component-error-text">{error}</FormHelperText>
      )}
    </FormControl>
  );
};

export default FieldText;
