import React from "react";
import {
  InputGroup,
  Input,
  FormFeedback,
  FormGroup
} from "reactstrap";

const InputRange = ({
  type = "text",
  placeholder,
  autoComplete,
  name,
  handleChange,
  handleBlur,
  error,
  touched,
  min,
  max,
  defaultValue
}) => {
  return (
    <FormGroup>
      <InputGroup className="mb-3">
        <Input
          type={type}
          placeholder={placeholder}
          autoComplete={autoComplete}
          name={name}
          onChange={handleChange}
          onBlur={handleBlur}
          invalid={error && touched}
          min={min}
          max={max}
          defaultValue={defaultValue}
        />
        <FormFeedback>{error}</FormFeedback>
      </InputGroup>
    </FormGroup>
  );
};

export default InputRange;
