import React from "react";
import ReactTagInput from "@pathofdev/react-tag-input";
import "@pathofdev/react-tag-input/build/index.css";

const InputTag = ({ name, value, setFieldValue, placeholder }) => {
  return (
    <ReactTagInput
      tags={value}
      placeholder={placeholder}
      onChange={newTags => setFieldValue(name, newTags)}
    />
  );
};

export default InputTag;
