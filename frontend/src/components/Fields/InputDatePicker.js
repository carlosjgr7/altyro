import React from "react";
import PropTypes from "prop-types";
import {
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Input,
  FormFeedback,
  FormGroup
} from "reactstrap";
import { primaryOvnix } from "../../styles/palette";

const InputDatePicker = ({
  type = "text",
  placeholder,
  autoComplete,
  name,
  handleChange,
  handleBlur,
  error,
  touched,
  iconName,
  position = "right"
}) => {
  return (
    <FormGroup>
      <InputGroup className="mb-3">
        {iconName && position !== "right" && (
          <InputGroupAddon addonType="prepend">
            <InputGroupText>
              <i className={iconName}></i>
            </InputGroupText>
          </InputGroupAddon>
        )}
        <Input
          type={type}
          placeholder={placeholder}
          autoComplete={autoComplete}
          name={name}
          onChange={handleChange}
          onBlur={handleBlur}
          invalid={error && touched}
        />
        {iconName && position === "right" && (
          <InputGroupAddon addonType="prepend">
            <InputGroupText>
              <i className={iconName} style={{ color: primaryOvnix }}></i>
            </InputGroupText>
          </InputGroupAddon>
        )}
        <FormFeedback>{error}</FormFeedback>
      </InputGroup>
    </FormGroup>
  );
};

InputDatePicker.propTypes = {
  name: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  autoComplete: PropTypes.string,
  handleChange: PropTypes.func.isRequired,
  handleBlur: PropTypes.func,
  error: PropTypes.string,
  touched: PropTypes.bool,
  iconName: PropTypes.string
};

InputDatePicker.defaultProps = {
  disabled: false,
  autoComplete: "off",
  iconName: "fa fa-calendar",
  type: "date"
};

export default InputDatePicker;
