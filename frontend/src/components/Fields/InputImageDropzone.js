import React, { useCallback } from "react";
import { useDropzone } from "react-dropzone";
import Thumb from "../ui/Thumb";
import { Row, Col } from "reactstrap";

const dropzoneStyle = {
  width: "100%",
  height: "auto",
  borderWidth: 2,
  borderColor: "rgb(102, 102, 102)",
  borderStyle: "dashed",
  borderRadius: 5,
  padding: 20
};

const InputImageDropzone = ({
  name,
  setFieldValue,
  values,
  error,
  touched
}) => {
  const onDrop = useCallback(
    acceptedFiles => {
      setFieldValue(name, [...values, ...acceptedFiles]);
    },
    [name, setFieldValue, values]
  );

  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });
  return (
    <div className="form-group" style={dropzoneStyle}>
      <div {...getRootProps()}>
        <input {...getInputProps()} />
        {isDragActive ? (
          <span>Suelta los archivos aquí ...</span>
        ) : (
          values.length <= 0 && (
            <span>
              Arrastre y suelte las fotos aquí, o haga clic para seleccionar la
              foto
            </span>
          )
        )}
        <Row>
          {values.map((file, i) => (
            <Col key={i} xs="auto">
              <Thumb
                file={file}
                setFieldValue={setFieldValue}
                name={name}
                values={values}
              />
            </Col>
          ))}
        </Row>
      </div>
      {error && touched && (
        <p
          className="text-danger"
          style={{ marginTop: "0.25rem", fontSize: "80%" }}
        >
          {error}
        </p>
      )}
    </div>
  );
};

export default InputImageDropzone;
