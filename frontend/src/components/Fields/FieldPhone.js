import React from "react";
import { FormGroup } from "reactstrap";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";

const InputPhone = ({ name, error, touched, value = "56", setFieldValue, disabled }) => {
  return (
    <FormGroup>
      <PhoneInput
        country={"cl"}
        value={value.toString()}
        onChange={phone => {
          setFieldValue(name, phone.replace(/ /g, ""))
        }}
        inputStyle={{
          width: "100%",
          borderColor: error && touched ? "#f86c6b" : "#e4e7ea"
        }}
        buttonStyle={{ backgroundColor: "#f0f3f5", borderColor: "#e4e7ea" }}
        disabled={disabled}
      />
      {error && touched && (
        <p
          className="text-danger"
          style={{ marginTop: "0.25rem", fontSize: "80%" }}
        >
          {error}
        </p>
      )}
    </FormGroup>
  );
};

export default InputPhone;
