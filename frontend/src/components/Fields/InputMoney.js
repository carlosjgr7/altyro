import React from "react";
import {
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Input,
  FormFeedback,
  FormGroup
} from "reactstrap";
import CurrencyFormat from "react-currency-format";

const InputMoney = ({ name, error, iconName, value, setFieldValue }) => {
  return (
    <FormGroup>
      <InputGroup className="mb-3">
        {iconName && (
          <InputGroupAddon addonType="prepend">
            <InputGroupText>
              <i className={iconName}></i>
            </InputGroupText>
          </InputGroupAddon>
        )}
        <CurrencyFormat
          value={value}
          thousandSeparator={true}
          prefix={"$"}
          customInput={Input}
          onValueChange={values => {
            const { value } = values;
            setFieldValue(name, value);
          }}
        />
        <FormFeedback>{error}</FormFeedback>
      </InputGroup>
    </FormGroup>
  );
};

export default InputMoney;
