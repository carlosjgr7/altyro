import React from "react";
import { InputGroup, Input, FormGroup, Label } from "reactstrap";

const InputRadio = ({
  type = "radio",
  placeholder,
  autoComplete,
  name,
  setFieldValue,
  handleBlur,
  error,
  touched,
  value,
  disabled,
  extraStyle = {},
  label,
  condition,
}) => {
  return (
    <FormGroup tag="fieldset">
      <FormGroup check>
        <InputGroup className="mb-3">
          <div className="col-12 p-0 m-0">
            <Label check>
              <Input
                type={type}
                placeholder={placeholder}
                autoComplete={autoComplete}
                name={name}
                onChange={() => setFieldValue(name, value)}
                onBlur={handleBlur}
                invalid={error && touched}
                disabled={disabled}
                style={extraStyle}
                value={value}
                checked={condition}
              />

              {typeof label === "object" ? label : ` ${label}`}
            </Label>
          </div>

          {error && (
            <div
              style={{
                backgroundColor: "#FF313B",
                width: "100%",
                height: 1.5,
                marginTop: 3,
              }}
            />
          )}
        </InputGroup>
      </FormGroup>
    </FormGroup>
  );
};

export default InputRadio;
