import React from "react";
import {
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Input,
  FormFeedback,
  FormGroup
} from "reactstrap";

const InputSelect = ({
  type = "text",
  placeholder,
  autoComplete,
  name,
  handleChange,
  handleBlur,
  error,
  touched,
  iconName,
  children,
  multiple,
  value
}) => {
  return (
    <FormGroup>
      <InputGroup className="mb-3">
        {iconName && (
          <InputGroupAddon addonType="prepend">
            <InputGroupText>
              <i className={iconName}></i>
            </InputGroupText>
          </InputGroupAddon>
        )}
        <Input
          type={type}
          placeholder={placeholder}
          autoComplete={autoComplete}
          name={name}
          onChange={handleChange}
          onBlur={handleBlur}
          invalid={error && touched}
          multiple={multiple}
          value={value}
        >
          {children}
        </Input>
        <FormFeedback>{error}</FormFeedback>
      </InputGroup>
    </FormGroup>
  );
};

export default InputSelect;
