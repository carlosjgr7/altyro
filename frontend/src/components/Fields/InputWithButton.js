import React from "react";
import {
  FormGroup,
  Col,
  InputGroup,
  Input,
  InputGroupAddon,
  Button
} from "reactstrap";
import { primaryOvnix } from "../../styles/palette";

const InputWithButton = ({
  type = "text",
  placeholder,
  autoComplete,
  name,
  handleChange,
  handleBlur,
  error,
  touched,
  right,
  iconButton = "fa fa-search",
  textButton = "Buscar",
  colorButton = "primary",
  handleSubmit
}) => {
  const InputRender = (
    <Input
      key={`${name}_input`}
      type={type}
      placeholder={placeholder}
      autoComplete={autoComplete}
      name={name}
      onChange={handleChange}
      onBlur={handleBlur}
      invalid={error && touched}
    />
  );
  const ButtonRender = (
    <InputGroupAddon addonType="prepend" key={`${name}_button`}>
      <Button type="button" color={colorButton} onClick={handleSubmit} style={{ backgroundColor: primaryOvnix }}>
        <i className={iconButton}></i> {textButton}
      </Button>
    </InputGroupAddon>
  );
  const renders = right
    ? [InputRender, ButtonRender]
    : [ButtonRender, InputRender];
  return (
    <FormGroup row>
      <Col md="12">
        <InputGroup>{renders.map(render => render)}</InputGroup>
      </Col>
    </FormGroup>
  );
};

export default InputWithButton;
