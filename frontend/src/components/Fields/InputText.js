import React from "react";
import {
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Input,
  FormFeedback,
  FormGroup,
  Label,
} from "reactstrap";

const InputText = ({
  type = "text",
  placeholder,
  autoComplete,
  name,
  handleChange,
  handleBlur,
  error,
  touched,
  iconName,
  value,
  disabled,
  extraStyle = {},
  label,
  min = null,
}) => {
  return (
    <FormGroup>
      {label && <Label for="exampleEmail">{label}</Label>}
      <InputGroup className="mb-3">
        {iconName && (
          <InputGroupAddon addonType="prepend">
            <InputGroupText>
              <i className={iconName}></i>
            </InputGroupText>
          </InputGroupAddon>
        )}
        <Input
          type={type}
          placeholder={placeholder}
          autoComplete={autoComplete}
          name={name}
          onChange={handleChange}
          onBlur={handleBlur}
          invalid={error && touched}
          disabled={disabled}
          style={extraStyle}
          value={value}
          min={min}
        />
        <FormFeedback>{error}</FormFeedback>
      </InputGroup>
    </FormGroup>
  );
};

export default InputText;
