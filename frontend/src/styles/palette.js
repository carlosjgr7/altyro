// Ovnix colors
export const blueOvnix = '#00AAFF';
export const redOvnix = '#EF5B27';
export const orangeOvnix = '#F6921E';
export const greenOvnix = '#37B34A';
export const purpleOvnix = '#7F3F97';
export const brownOvnix = '#C3996B';
export const redError = '#F15858';
export const primaryOvnix = '#a200bb';
// TEXT
export const textColor = '#ACACAC';
export const grayOvnix = '#D5D5D5';
export const titleColor = '#707070';

//border
export const borderGreen = '#09FF47';

export const backgroundDefault = '#F6F6F6';

export const greenAqua = '#B5FBC7';

export const yellow = '#ffff6c';
export const secondaryOvnix = '#ffff6c';

export const pastelBlue = '#2e86d9';
