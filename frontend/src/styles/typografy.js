
import { Platform } from 'react-native';

export const Black = 'ExtraBold';
export const Bold = 'Bold';
export const Light = 'Light';
export const Regular = 'Regular';
export const Italic = 'Italic';
export const Medium = 'Medium';
export const Thin = 'Thin';

export const fontMaker = (weight = Regular, family = 'Roboto') => {
  return {
    fontFamily: Platform.OS === 'ios' ? 'System' : `${family}-${weight}`,
  };
};
