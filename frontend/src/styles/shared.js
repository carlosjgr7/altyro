import {StyleSheet} from 'react-native';
export default StyleSheet.create({
    backIcon: {fontWeight: '100', fontSize: 20},
    viewCentered: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    marginTop20: {marginTop: 20},
    padding20: {padding: 20},
    container: {backgroundColor: '#f2f2f2'},
    acceptButton: {
      backgroundColor: '#07F042',
      borderRadius: 50,
      justifyContent: 'center',
    },
    acceptButtonText: {
      color: '#000',
    },
    marginLeft20: {marginLeft: 20,},
    primaryBgColor: {
      backgroundColor: '#F8E71C',
    },
    horizontalAlign: { flex: 1, flexDirection: 'row' },
    textCenter: {textAlign: 'center'},
    isRequesting: {backgroundColor: 'gray',}
});
