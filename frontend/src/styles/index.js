import {fontMaker} from './typografy';
import {textColor} from './palette';

export const spaceDefault = 14.67;
export const hitSlop = {top: 25, bottom: 25, left: 25, right: 5};

export const defaultText = (
  fontSize,
  textAlign = 'left',
  color = textColor,
  fontType,
) => ({
  fontSize,
  textAlign,
  color,
  ...fontMaker(fontType),
});
export const elevationShadowStyle = elevation => ({
  elevation,
  shadowColor: 'black',
  shadowOffset: {width: 0, height: 0.5 * elevation},
  shadowOpacity: 0.3,
  shadowRadius: 0.8 * elevation,
});
