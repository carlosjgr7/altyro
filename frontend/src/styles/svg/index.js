import OnlineShop from './OnlineShop';
import Discount from './Discount';
import MarketPlace from './MarketPlace';
import CallCenter from './CallCenter';
const IconSvg = {
  OnlineShop,
  Discount,
  MarketPlace,
  CallCenter,
};
export default IconSvg;
