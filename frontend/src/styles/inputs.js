import {StyleSheet} from 'react-native';
export default StyleSheet.create({
  input: {
    backgroundColor: '#fff',
    borderColor: '#07F042',
    borderRadius: 5,
    borderWidth: 1,
    height: 45,
    fontSize: 15,
    paddingLeft: 10,
  },
  inputError: {
    borderColor: 'red',
  },
  inputSuccess: {
    borderColor: 'green',
    color: 'green',
  },
});
