import { PREFIXPANEL } from "./../constants";
const lateralPanel = {
  items: [
    {
      name: "Dashboard",
      url: "/panel",
      icon: "fa fa-cog"
    },
    {
      title: true,
      name: "Menu",
      wrapper: {
        element: "",
        attributes: {}
      }
    },
    {
      name: "Ingresar pedido",
      url: `${PREFIXPANEL}/order/new`,
      icon: "fa fa-plus"
    },
    {
      name: "Mis pedidos",
      url: `${PREFIXPANEL}/orders`,
      icon: "fa fa-list"
    },
    {
      name: "Mis datos",
      url: `${PREFIXPANEL}/profile`,
      icon: "fa fa-address-card"
    }
  ]
};

export default lateralPanel;
