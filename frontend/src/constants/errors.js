const errors = {
  NotAuthorizedException: "Contraseña incorrecta",
  UserNotFoundException: "Este usuario no existe",
  UserNotConfirmedException: "Su usuario debe estar verificado",
};

export default errors;
