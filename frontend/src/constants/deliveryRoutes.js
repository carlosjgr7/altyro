import { PREFIXDELIVERY } from ".";

const DeliveryRoutes = {
  DELIVERY: "/delivery",
  SIGN_IN: "/signIn",
  SIGN_UP: "/signUp",
  DASHBOARD: `${PREFIXDELIVERY}/dashboard`,
  DELIVERIES: `${PREFIXDELIVERY}/delivery`
};
export default DeliveryRoutes;
