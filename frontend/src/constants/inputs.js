/* eslint-disable no-useless-escape */
// expresiones regulares
export const validateEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
export const validatePhone = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{5}$/;
export const validatePassword = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/;

// Cadenas de errores
export const errorRequired = "Este campo es requerido";
export const errorNumeric = "Este campo es numerico";
export const errorEmail = "Este campo es de tipo email";
export const errorPhone = "Este número no coindice con un formato telefonico";
export const errorPassword =
  "La debe ser de 8 digitos y debe contener al menos una mayuscula, una minuscula y un número";
