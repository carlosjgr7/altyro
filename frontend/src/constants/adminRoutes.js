import { PREFIXADMIN } from ".";

const AdminRoutes = {
  ADMIN: "/admin",
  SIGN_IN: "/signIn",
  SIGN_UP: "/signUp",
  DASHBOARD: `${PREFIXADMIN}/dashboard`,
  DASHBOARD_USERS: `${PREFIXADMIN}/dashboard/users`,
  SHOP_REQUEST: `${PREFIXADMIN}/requests/shops`,
  PRODUCTS: `${PREFIXADMIN}/products`,
  STORIES: `${PREFIXADMIN}/stories`,
  INVALID_USER: `${PREFIXADMIN}/invalidUser`,
};

export default AdminRoutes;
