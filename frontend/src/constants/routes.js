import { PREFIXPANEL } from ".";

const Routes = {
  COMPANY: "/company",
  SIGN_IN: "/signIn",
  SIGN_UP: "/signUp",
  TERMS_CONDITIONS: "/terms-conditions",
  CODE: "/code/:statusCode/:email",
  FORGOT_PASSWORD: `/code/:statusCode`,
  DASHBOARD: `${PREFIXPANEL}/dashboard`,
  SALES: `${PREFIXPANEL}/sales`,
  SALES_REQUESTED: `${PREFIXPANEL}/sales?status=requested`,
  USERS: `${PREFIXPANEL}/users`,
  NEW_USER: `${PREFIXPANEL}/users/new`,
  PRODUCTS: `${PREFIXPANEL}/products`,
  NEW_PRODUCT: `${PREFIXPANEL}/products/new`,
  EDIT_PRODUCT: `${PREFIXPANEL}/products/:id/edit`,
  STORIES: `${PREFIXPANEL}/stories`,
  NEW_STORY: `${PREFIXPANEL}/story/new`,
  EDIT_STORY: `${PREFIXPANEL}/stories/:id/edit`,
  BRANCHES: `${PREFIXPANEL}/branches`,
  PROFILE: `${PREFIXPANEL}/profile`,
  HELP: `${PREFIXPANEL}/help`,
  CHANGE_PASSWORD: `${PREFIXPANEL}/change/password`,
};
export default Routes;
