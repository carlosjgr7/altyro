const { Router } = require('express');
const router = Router();

// respuestas || procesos
const { login, register } = require('../controllers/auth');

// rutas
router.get('/', login);

router.post('/register', register);

module.exports = router;
