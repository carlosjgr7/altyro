const express = require('express');
var cors = require('cors');
const app = express();

// initialize
const port = process.env.port || 3030;

// settings

// middlewares
app.use(
	cors({
		allowedHeaders: ['sessionId', 'Content-Type'],
		exposedHeaders: ['sessionId'],
		origin: '*',
		methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
		preflightContinue: false,
	})
);
// gloval var

// routes
const rutas = require('./routes/index.routes');
app.use(rutas);

// run

app.listen(port, () => {
	console.log(
		'                                                                  ()_()'
	);
	console.log(
		`app corriendo en el puerto http://localhost:${port} leoM             (o.o)`
	);
	console.log(
		'                                                                  (|_|)*'
	);
});

//Run app, then load http://localhost:port in a browser to see the output.
