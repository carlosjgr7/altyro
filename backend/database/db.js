const mysql = require('mysql');

const db = mysql.createConnection({
	host: 'localhost',
	user: 'root',
	password: '',
	database: 'altyro',
});

db.connect(function (err) {
	if (err) console.log('no conectado!');
	else console.log('Connected!');
});

module.exports = db;
